package employees;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

// MongoRepository gives us access to "by convention" function names
// EmployeeRepositoryCustom will allow us to create complex queries.
public interface EmployeeRepository extends MongoRepository<Employee, String>, EmployeeRepositoryCustom  {

    public List<Employee> findByName(String name);

    // Associate specific query with a method.
    @Query(value="{ name: ?0 }", fields="{name: 1, _id: 0}")
	public List<Employee> queryAttributeExample(String name);
}
