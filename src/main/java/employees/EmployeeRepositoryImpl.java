package employees;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.WriteResult;

public class EmployeeRepositoryImpl implements EmployeeRepositoryCustom {

	@Autowired
	MongoTemplate mongoTemplate;
	
	
	
	// Use MongoTemplate to create a "custom" query.  Find the first Pat.
	public Employee findPat() {
		
		Query query = new Query(Criteria.where("name").is("Pat"));
		
		List<Employee> result = mongoTemplate.find(query, Employee.class); 
		
		return result.size() > 0 ? result.get(0) : null;
	}
	
	
	
	// Update db based on age criteria.
	public List<Employee> updateExample() {
		
		Query query1 = new Query(Criteria.where("age").gt(40).lt(50));
		
		Update update = new Update();
		update.set("isActive", false);
		
		WriteResult wResult = mongoTemplate.updateMulti(query1, update, Employee.class);
		
		List<Employee> result = new ArrayList<Employee>();
		
		if (wResult.getN() > 0) {
			Query query2 = new Query(Criteria.where("isActive").is(false));
			result = mongoTemplate.find(query2, Employee.class);
		}
		
		return result;
	}
	
	
	
	// Programatically aggregate via matching and projection.
	public List<Employee> aggregationExample() {
		
		ProjectionOperation project = Aggregation.project("name");
		
		Criteria isPat = new Criteria("name").is("Pat");
		Criteria isJohn = new Criteria("name").is("John");
		MatchOperation match = Aggregation.match(new Criteria().orOperator(isPat, isJohn));
		
		Aggregation aggregation = Aggregation.newAggregation(match, project);
		
		AggregationResults<Employee> aggResults = mongoTemplate.aggregate(aggregation, "employee", Employee.class);
		List<Employee> results = aggResults.getMappedResults();
				
		return results;
	}
}
