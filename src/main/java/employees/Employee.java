package employees;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {

    @Id
    public String id;

    public String name;
    public List<Interest> interests;
    public int age;
    public boolean isActive;

    public Employee() {
        this("", 0, null);
    }

    public Employee(String name) {
        this(name, 0, null);
    }

    public Employee(String name, List<Interest> interests) {
    	this(name, 0, interests);
    }
    
    public Employee(String name, int age, List<Interest> interests) {
    	this(name, age, interests, true);
    }
    
    private Employee(String name, int age, List<Interest> interests, boolean isActive) {
    	this.name = name;
    	this.age = age;
    	this.interests = interests != null ? interests : new ArrayList<Interest>();
    	this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "Employee{" + " name = '" + name + "', interests = " + interests + "}";
    }


}
