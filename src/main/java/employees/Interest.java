package employees;

public class Interest {

    public String interest;

    public Interest() { }
    
    public Interest(String interest) {
        this.interest = interest;
    }

    @Override
    public String toString() {
        return "Interest{ interest = '" + interest + "'}";
    }
}
