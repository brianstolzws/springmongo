package employees;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterestApplication implements CommandLineRunner {

    @Autowired
    private EmployeeRepository repository;

    @Override
    public void run(String... args) throws Exception {

    	System.out.println("Starting!");
    	
    	// Remove documents from last run
        repository.deleteAll();

        // John is boring, no interests
        repository.save(new Employee("John", 45, null));

        // Include some interests
        List<Interest> interests = new ArrayList<>();
        interests.add(new Interest("Java, C#"));
        interests.add(new Interest("Spring, React"));
        repository.save(new Employee("Emily", 23, interests));
        
        // Data for Pat for custom query.
        List<Interest> patInterests = new ArrayList<>();
        patInterests.add(new Interest("Traveling"));
        patInterests.add(new Interest("Woodworking"));
        repository.save(new Employee("Pat", 34, patInterests));

        // Get all employees
        System.out.println("Employees via findAll():");
        for (Employee employee : repository.findAll()) {
            System.out.println(employee);
        }
        System.out.println();

        // Get one employee
        System.out.println("Frist employee via findByFirstName('John'):");
        System.out.println(repository.findByName("John"));
    }
	
    public static void main(String[] args) {
        SpringApplication.run(InterestApplication.class, args);
    }

}
