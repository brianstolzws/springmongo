package employees;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InterestController {

    @Autowired
    private EmployeeRepository repository;

    @RequestMapping("/pat")
    public Employee getPat() {
    	return repository.findPat();
    }
    
    @RequestMapping("/employees/{name}")
    public List<Employee> getRecognition(@PathVariable("name") String name){
        return repository.findByName(name);
    }

    @RequestMapping("/employees")
    public List<Employee> getColleagues(){
        return repository.findAll();
    }
    
    @RequestMapping("/test/aggregation")
    public List<Employee> getTestAggregation() {
    	return repository.aggregationExample();
    }
    
    @RequestMapping("/test/queryAttribute")
    public List<Employee> getTestQueryAttribute(@RequestParam(value = "name", required = true) String name) {
    	return repository.queryAttributeExample(name);
    }
    
    @RequestMapping("/test/update")
    public List<Employee> getTestUpdateExample() {
    	return repository.updateExample();
    }

    @PostMapping("/employees")
    public ResponseEntity<String> addColleague(@RequestBody Employee colleague){
        repository.save(colleague);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/employees/{name}")
    public ResponseEntity<String> deleteColleague(@PathVariable String name){
        List<Employee> employees = repository.findByName(name);
        if(employees.size() == 1) {
            Employee colleague = employees.get(0);
            repository.delete(colleague);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
