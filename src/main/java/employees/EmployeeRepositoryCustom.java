package employees;

import java.util.List;

// Interface for custom mongo queries
public interface EmployeeRepositoryCustom {

	public Employee findPat();
	public List<Employee> updateExample();
	public List<Employee> aggregationExample();
}
